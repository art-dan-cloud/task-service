package com.artdan.cloud.artdancloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ArtDanCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtDanCloudApplication.class, args);
	}
}
